using System.Text;
using System.Text.Json;

namespace PictureDb;

public class PpmImageStrategy<T> where T: class
{
    private readonly int _width = 512;
    private readonly int _height = 512;
    private readonly byte[] STOP_MARK = { 0x32, 0x35, 0x35, 0x0A };
    
    public T Load(string fileName)
    {
        byte[] buffer = new byte[STOP_MARK.Length];
        byte[] dataBuffer = new byte[1024];
        (int readResult, int headerPos) = (1, 0);
        List<byte> result = new List<byte>();

        using (BinaryReader br = new BinaryReader(new FileStream(fileName, FileMode.Open, FileAccess.Read)))
        {
            while (readResult > 0)
            {
                br.BaseStream.Seek(headerPos, SeekOrigin.Begin);
                readResult = br.Read(buffer, 0, STOP_MARK.Length);

                if (CheckStopMark(buffer))
                {
                    Console.WriteLine("Yes! "+headerPos);
                    break;
                }

                headerPos++;
            }

            br.BaseStream.Seek(headerPos+STOP_MARK.Length, SeekOrigin.Begin);
            
            //Read data
            while (readResult > 0)
            {
                readResult = br.Read(dataBuffer, 0, 1024);
                var allBlank = dataBuffer.All(e => e == 0xFF);
                if(allBlank) break;
                
                result.AddRange(dataBuffer);
            }
        }

        var chars = Encoding.ASCII.GetChars(result.Where(b => b != 0xFF).ToArray());
        var str = new string(chars);

        var r = JsonSerializer.Deserialize<T>(str);
        return r;
    }

    public void Save(T db, string fileName)
    {
        using (var writer = new StreamWriter(fileName))
        {
            writer.WriteLine("P5");
            writer.WriteLine($"{_width} {_height}");
            writer.WriteLine("255");
            writer.Close();
        }

        using (var binaryWriter = new FileStream(fileName, FileMode.Append))
        {
            var text = JsonSerializer.Serialize(db);
            var bytes = Encoding.ASCII.GetBytes(text);
            var data = GetImageData(bytes);
            
            binaryWriter.Write(data);
            binaryWriter.Close();
        }
    }
    
    private bool CheckStopMark(byte[] buffer)
    {
        for (int i = 0; i < STOP_MARK.Length; i++)
        {
            if (buffer[i] != STOP_MARK[i]) return false;
        }

        return true;
    }

    private byte[] GetImageData(byte[] dbData)
    {
        byte white = (byte) 255;
        byte[] result = new byte[_width * _height];
        int idx = 0;
        
        for (int y = 0; y < _height; y++)
        {
            for (int x = 0; x < _width; x++)
            {
                result[idx] = (idx > dbData.Length - 1) ? white : dbData[idx];
                idx++;
            }
        }

        return result;
    }
}