﻿using System;
using System.Text.Json;

namespace PictureDb;

public static class Program
{
    public static void Main(string[] args)
    {
        var d = new Dictionary<string, string>() {{"hello", "welt"}, {"Test", "Hello, world!!!!!!"}};
        var ps = new PpmImageStrategy<Dictionary<string, string>>();
        ps.Save(d, "helloNew.ppm");
        var r = ps.Load("helloNew.ppm");
    }
}
